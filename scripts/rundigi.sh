#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} HIT.root"
    exit 1
fi

HITS_INPUT=${1}
RDO_OUTPUT=${HITS_INPUT//HITS/RDO}
echo ${RDO_OUTPUT}

if [ ${HITS_INPUT} -ot ${RDO_OUTPUT} ]; then
    echo "Output newer than input. Already processed?"
    exit 0
fi

OUTDIR=$(dirname ${RDO_OUTPUT})
if [ ! -d ${OUTDIR} ]; then
    mkdir -p ${OUTDIR}
fi

# r13051, asetup Athena,22.0.41.3
export ATHENA_CORE_NUMBER=8
Reco_tf.py \
    --asetup 'RDOtoRDOTrigger:Athena,21.0.124' \
    --athenaopts 'HITtoRDO:--nprocs=$ATHENA_CORE_NUMBER HITtoRDO:--threads=0 RDOMergeAthenaMP0:--threads=0' \
    --autoConfiguration 'everything' \
    --conditionsTag 'default:OFLCOND-MC16-SDR-RUN2-08' \
    --digiSteeringConf 'StandardInTimeOnlyTruth' \
    --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink","ELReset "]' \
               'ESDtoAOD:fixedAttrib=[s if "CONTAINER_SPLITLEVEL = \'99\'" not in s else "" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' \
    --postInclude 'default:PyJobTransforms/UseFrontier.py' \
    --preExec 'POOLMergeAthenaMPESD0:from RecExConfig.RecFlags import rec; rec.doTrigger=True;' \
              'all:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(False);' \
              'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False);' \
              'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doMinBias.set_Value_and_Lock(True);' \
    --preInclude 'all:Campaigns/MC16NoPileUp.py' \
    --steering 'doRDO_TRIG' \
    --triggerConfig 'RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'RAWtoESD=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'ESDtoAOD=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'POOLMergeAthenaMPESD0=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
    --valid 'True' \
    --inputHITSFile ${HITS_INPUT} \
    --outputRDOFile events.RDO.root

mv events.RDO.root ${RDO_OUTPUT}
