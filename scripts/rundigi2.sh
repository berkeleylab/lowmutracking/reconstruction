#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} RDO.root"
    exit 1
fi

RDO_INPUT=${1}
RDOTRIG_OUTPUT=${RDO_INPUT//RDO/RDO_TRIG}

if [ ${RDO_INPUT} -ot ${RDOTRIG_OUTPUT} ]; then
    echo "Output newer than input. Already processed?"
    echo "  ${RDO_TRIG_OUTPUT}"
    exit 0
fi

OUTDIR=$(dirname ${RDOTRIG_OUTPUT})
if [ ! -d ${OUTDIR} ]; then
    mkdir -p ${OUTDIR}
fi

# r12253, asetup Athena,21.0.54.3
Reco_tf.py \
    --autoConfiguration 'everything' \
    --conditionsTag 'default:OFLCOND-MC16-SDR-RUN2-02' \
    --digiSteeringConf 'StandardSignalOnlyTruth' \
    --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' \
               'ESDtoAOD:fixedAttrib=[s if "CONTAINER_SPLITLEVEL = \'99\'" not in s else "" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' \
    --postInclude 'default:PyJobTransforms/UseFrontier.py' \
    --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' \
              'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODFULL");' \
    --steering 'doRDO_TRIG' \
    --triggerConfig 'RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
    --valid 'True' \
    --inputRDOFile ${RDO_INPUT} \
    --outputRDO_TRIGFile ${RDOTRIG_OUTPUT}
