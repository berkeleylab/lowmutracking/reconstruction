#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} RDO_TRIG.root"
    exit 1
fi

RDOTRIG_INPUT=${1}
AOD_OUTPUT=${RDOTRIG_INPUT//RDO_TRIG/AOD}

if [ ${RDOTRIG_INPUT} -ot ${AOD_OUTPUT} ]; then
    echo "Output newer than input. Already processed?"
    exit 0
fi

OUTDIR=$(dirname ${AOD_OUTPUT})
if [ ! -d ${OUTDIR} ]; then
    mkdir -p ${OUTDIR}
fi

# r13051, asetup Athena,22.0.41.3
export ATHENA_CORE_NUMBER=8
Reco_tf.py \
    --asetup 'RDOtoRDOTrigger:Athena,21.0.124' \
    --athenaopts 'HITtoRDO:--nprocs=$ATHENA_CORE_NUMBER HITtoRDO:--threads=0 RDOMergeAthenaMP0:--threads=0' \
    --autoConfiguration 'everything' \
    --conditionsTag 'default:OFLCOND-MC16-SDR-RUN2-08' \
    --digiSteeringConf 'StandardInTimeOnlyTruth' \
    --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
    --numberOfCavernBkg '0' \
    --numberOfHighPtMinBias '0.177' \
    --numberOfLowPtMinBias '90.323' \
    --pileupFinalBunch '6' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink","ELReset "]' \
               'ESDtoAOD:fixedAttrib=[s if "CONTAINER_SPLITLEVEL = \'99\'" not in s else "" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' \
               'ESDtoAOD:from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg;xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);' \
               'ESDtoDPD:from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg;xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);' \
               'RAWtoESD:from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg;xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);' \
    --postInclude 'default:PyJobTransforms/UseFrontier.py' \
    --preExec 'POOLMergeAthenaMPESD0:from RecExConfig.RecFlags import rec; rec.doTrigger=True;' \
              'all:userRunLumiOverride={"run":310000, "startmu":0.0, "endmu":80.0, "stepmu":1.0, "startlb":1, "timestamp": 1550000000};' \
              'all:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(False);' \
              'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False);' \
    --preInclude 'all:Campaigns/MC20e.py' \
                 'HITtoRDO:Campaigns/PileUpMC20e.py' \
    --steering 'doRDO_TRIG' \
    --triggerConfig 'RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'RAWtoESD=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'ESDtoAOD=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
                    'POOLMergeAthenaMPESD0=MCRECO:DBF:TRIGGERDBMC:2179,51,207' \
    --valid 'True' \
    --inputRDO_TRIGFile ${RDOTRIG_INPUT} \
    --outputAODFile events.AOD.root

mv events.AOD.root ${AOD_OUTPUT}
