#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} EVNT.root"
    exit 1
fi

EVNT_INPUT=${1}
HITS_OUTPUT=${EVNT_INPUT//EVNT/HITS}

if [ ${EVNT_INPUT} -ot ${HITS_OUTPUT} ]; then
    echo "Output newer than input. Already processed?"
    exit 0
fi

# s3481, asetup AtlasOffline,21.0.15
Sim_tf.py --maxEvents 1000 \
    --DBRelease 'all:current' \
    --DataRunNumber '284500' \
    --conditionsTag 'default:OFLCOND-MC16-SDR-14' \
    --geometryVersion 'default:ATLAS-R2-2016-01-00-01_VALIDATION' \
    --physicsList 'FTFP_BERT_ATL_VALIDATION' \
    --postExec 'EVNTtoHITS:from AthenaCommon.AppMgr import ToolSvc; from AthenaCommon.SystemOfUnits import MeV; ToolSvc.ISF_MCTruthStrategyGroupID_MC15.ParentMinPt=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupID_MC15.ChildMinPt=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupIDHadInt_MC15.ParentMinPt=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupIDHadInt_MC15.ChildMinPt=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupCaloMuBrem.ParentMinEkin=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupCaloMuBrem.ChildMinEkin=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupCaloDecay_MC15.ParentMinEkin=10.*MeV; ToolSvc.ISF_MCTruthStrategyGroupCaloDecay_MC15.ChildMinEkin=10.*MeV' \
    --postInclude 'default:RecJobTransforms/UseFrontier.py' \
    --preExec 'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' \
              'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' \
    --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' \
    --simulator 'FullG4' \
    --truthStrategy 'MC15aPlus' \
    --inputEVNTFile ${EVNT_INPUT} \
    --outputHITSFile ${HITS_OUTPUT}
