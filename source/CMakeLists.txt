cmake_minimum_required( VERSION 3.18 )
project( UserAnalysis VERSION 0.0.1 LANGUAGES CXX )

find_package( Athena 22.0 REQUIRED )

atlas_ctest_setup()

atlas_project( USE Athena ${Athena_VERSION} )

lcg_generate_env( SH_FILE
  ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
  DESTINATION . )

atlas_cpack_setup()
